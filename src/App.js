import React, {Component} from 'react';
import './App.less';
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";
import Home from "./components/Home";
import Details from "./components/Details";
import Add from "./components/Add";

class App extends Component {
    render() {
        return (
            <Router>
                <Switch>
                    <Route exact path='/' component={Home}/>
                    <Route path='/notes/:id' component={Details}/>
                    <Route path='/add' component={Add}/>
                </Switch>
            </Router>
        );
    }
}

export default App;
