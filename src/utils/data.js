const getNote = (url) => fetch(url).then(response => response.json());

const createNote = (url, body) => fetch(
    url,
    {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(body)
    }
);

const deleteNote = (url, id) => fetch(
    `${url}/${id}`,
    {
        method: 'DELETE'
    }
);


export {
    getNote, createNote, deleteNote

};


