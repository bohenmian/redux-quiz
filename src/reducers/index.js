import {combineReducers} from "redux";
import noteReducer from "./noteReducer";

const reducers = combineReducers({
    notes:noteReducer
});
export default reducers;
