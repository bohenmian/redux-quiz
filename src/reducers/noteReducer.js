const initialState = {
    notes: [],
    isCreated: false,
    isDeleted: false,
    isRedirect: false
};

export default (state = initialState, action) => {
    switch (action.type) {
        case 'GET_NOTES':
            return {
                ...state,
                notes: action.payload
            };
        case 'CREATE_NOTE':
            return {
                ...state,
                notes: action.payload,
                isCreated: action.isCreated
            };
        case 'DELETE_NOTE':
            return {
                ...state,
                notes: action.payload,
                isDeleted: action.isDeleted
            };
        case 'RESET':
            return {
                ...state,
                isCreated: action.isCreated,
                isDeleted: action.isDeleted,
                isRedirect: action.isRedirect
            };
        default:
            return state;
    }
};
