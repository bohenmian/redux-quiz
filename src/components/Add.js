import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Redirect} from 'react-router-dom';
import {createNoteAction} from "../actions/notesActions";
import {bindActionCreators} from "redux";
import Header from "../pages/header/Header";
import './add.less';

class Add extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: '',
            description: '',
            disable: false

        };
        this.handleChange = this.handleChange.bind(this);
        this.handleClick = this.handleClick.bind(this);
    }

    handleChange(event, key) {
        const value = event.target.value;
        this.setState({
            [key]: value
        });
    }

    handleClick(status) {
        const {title, description} = this.state;
        // todo 实现title和description为空时disable禁用
        if (status === 'submit') {
            this.props.createNoteAction({title, description});
        } else if (status === 'cancel') {
            this.props.history.goBack();
        }
    }

    render() {
        const {title, description} = this.state;
        return (
            <>
                <Header/>
                <div className='add'>
                    {this.props.notes.isCreated ? <Redirect to='/'/> : null}
                    <h2>创建笔记</h2>
                    <hr/>
                    <section className='title'>标题</section>
                    <section>
                        <input value={title} type='text' onChange={e => this.handleChange(e, 'title')}/>
                    </section>
                    <section className='description'>正文</section>
                    <section>
                        <textarea value={description} onChange={e => this.handleChange(e, 'description')}/>
                    </section>
                    <section className='button'>
                        <button className='submit' disabled={this.state.disable}
                                onClick={() => this.handleClick('submit')}>提交
                        </button>
                        <button className='cancel' onClick={() => this.handleClick('cancel')}>取消</button>
                    </section>
                </div>
            </>
        );
    }
}


const mapStateToProps = state => ({
    notes: state.notes
});

const mapDispatchToProps = dispatch => bindActionCreators({
    createNoteAction
}, dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(Add)
