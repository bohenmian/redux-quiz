import React, {Component} from 'react';
import Header from "../pages/header/Header";
import {connect} from "react-redux";
import {deleteNoteAction, getNotesAction, resetAction} from "../actions/notesActions";
import {Link, Redirect} from "react-router-dom";
import {bindActionCreators} from "redux";
import './details.less';

class Details extends Component {
    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick(status, id) {
        if (status === '删除') {
            this.props.deleteNoteAction(id);
        } else if (status === '返回') {
            // this.props.resetAction();
            this.props.history.goBack();
        }
    }

    render() {
        const id = this.props.match.params.id;
        const note = this.props.notes.notes.filter(note => note.id == id) || [];
        return (
            <>
                <Header/>
                <div className='note-details'>
                    {this.props.notes.isDeleted ? <Redirect to='/'/> : null}
                    <section className='note-sidebar'>
                        {
                            this.props.notes.notes.map(note => <Link key={note.id}
                                                                     to={`/notes/${note.id}`}>{note.title}</Link>)
                        }
                    </section>
                    <div className='note-detail'>
                        <section>
                            <h1>{note[0].title}</h1>
                            <hr/>
                            <p>{note[0].description}</p>
                        </section>
                    </div>

                    <section className='details-button'>
                        <button className='delete' onClick={() => this.handleClick('删除', id)}>删除</button>
                        <button className='return' onClick={() => this.handleClick('返回')}>返回</button>
                    </section>
                    {this.props.notes.isRedirect ? <Redirect to='/'/> : null}
                </div>
            </>
        );
    }
}

const mapStateToProps = state => ({
    notes: state.notes
});

const mapDispatchToProps = dispatch => bindActionCreators({
    deleteNoteAction, resetAction
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Details)
