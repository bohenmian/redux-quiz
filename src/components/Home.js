import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import {getNotesAction} from "../actions/notesActions";
import Header from "../pages/header/Header";
import {bindActionCreators} from "redux";
import './home.less';

class Home extends Component {

    componentDidMount() {
        this.props.getNotesAction();
    }

    render() {
        return (
            <div>
                <Header/>
                <section className='notes'>
                    {
                        this.props.notes.map(item =>
                            <section className='note-list'>
                                <Link className='note-link' key={item.id} to={`/notes/${item.id}`}>
                                    <span>{item.title}</span>
                                </Link>
                            </section>
                        )
                    }
                    <section className='note-list'>
                        <Link className='note-link' to='/add'><span>+</span></Link>
                    </section>
                </section>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    notes: state.notes.notes
});

const mapDispatchToProps = dispatch => bindActionCreators({
    getNotesAction
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Home);
