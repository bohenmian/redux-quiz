import {createNote, deleteNote, getNote} from "../utils/data";

export const getNotesAction = () => dispatch => {
    getNote('http://localhost:8080/api/posts').then(response => {
        dispatch({
            type: 'GET_NOTES',
            payload: response
        });
    }).catch(Error);
};

export const createNoteAction = (body) => dispatch => {
    createNote('http://localhost:8080/api/posts', body)
        .then(response => {
            if (response.status === 201) {
                getNote('http://localhost:8080/api/posts').then(response => {
                    dispatch({
                        type: 'CREATE_NOTE',
                        payload: response,
                        isCreated: true
                    });
                }).catch(Error);
            }
        })
};

export const deleteNoteAction = (id) => dispatch => {
    deleteNote('http://localhost:8080/api/posts', id)
        .then(response => {
            if (response.status === 200) {
                getNote('http://localhost:8080/api/posts').then(response => {
                    dispatch({
                        type: 'DELETE_NOTE',
                        payload: response,
                        isDeleted: true
                    });
                }).catch(Error);
            }
        });
};
export const resetAction = () => ({
    type: 'RESET',
    payload: {
        isCreated: false,
        isDeleted: false,
        isRedirect: true
    }
});
