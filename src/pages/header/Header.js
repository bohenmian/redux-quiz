import React from 'react';

import {MdEventNote} from 'react-icons/md';
import './header.less';

export default function Header() {
  return (
    <header>
      <MdEventNote />
      <span>NOTES</span>
    </header>
  );
}
